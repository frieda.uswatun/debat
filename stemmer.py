from Sastrawi.Stemmer.StemmerFactory import StemmerFactory

text_file = open("stemmed_input.txt", "w")
factory = StemmerFactory()
stemmer = factory.create_stemmer()
with open('input.txt', 'r') as myfile:
    data=myfile.read().replace('\n', '')
    myfile = stemmer.stem(data)
    text_file.write(myfile)
text_file.close()
