import io
from collections import Counter
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer

infile = 'input.txt'
file = open(infile).read()
myfile = [open(infile).read()]

ngram_vectorizer = CountVectorizer(analyzer='word', ngram_range=(1, 1), min_df=1)

X = ngram_vectorizer.fit_transform(myfile)
vocab = ngram_vectorizer.get_feature_names()
counts = X.sum(axis=0).A1
freq_distribution = Counter(dict(zip(vocab, counts)))


vectorizer = TfidfVectorizer()
vectorizer.fit(myfile)
# summarize
print(vectorizer.vocabulary_)
# print(vectorizer.idf_)
# # encode document
# vector = vectorizer.transform([myfile[0]])
# # summarize encoded vector
# print(vector.shape)
# print(vector.toarray())
